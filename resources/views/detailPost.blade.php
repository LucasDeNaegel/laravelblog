@extends('layouts.master')

@section('title')
    {{ $post->title }}
@endsection

@section('content')
    <article>
        <h2>{{$post->title}}</h2>
        <p class="date"> <strong>created at:</strong> {{ $post->created_at->format('m/d/Y') }}</p>
        {!! $post->content !!} 

        @if (count($post->media) > 0)
            <h3>Media:</h3>
            todo 
        @endif

        @if (count($post->refs) > 0)
            <h3>references:</h3>
            <ul>
                @foreach ($post->refs as $ref)
                <li>
                    {{$ref->apa}}
                    @if ($ref -> url)
                        <a href="{{ $ref -> url}}">{{ $ref -> url}}</a>
                    @endif
                </li>
                @endforeach
            </ul>
        @endif
    </article>
@endsection('content')