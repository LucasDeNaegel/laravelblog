@extends('layouts.master')

@section('title')
    login
@endsection

@section('content')
    <section>
        <h2>Login</h2>
        <x-jet-validation-errors class='error' />

        <form method="POST" action="{{ route('login') }}">
            @csrf


            <x-jet-label for="email" value="{{ __('Email') }}" />
            <x-jet-input id="email" type="email" name="email" :value="old('email')" required autofocus />



            <x-jet-label for="password" value="{{ __('Password') }}" />
            <x-jet-input id="password" type="password" name="password" required autocomplete="current-password" />


            <label for="remember_me">
                <input id="remember_me" type="checkbox" name="remember">
                <span>{{ __('Remember me') }}</span>
            </label>

            @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
            </a>
            @endif

            <x-jet-button>
                {{ __('Login') }}
            </x-jet-button>
        </form>
    </section>
@endsection