<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('assets/css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/screen.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    @yield('css')
    <title>blog - @yield('title')</title>
</head>

<body>
    <header>
        <h1>Blog <span>by Lucas De Naegel</span></h1>
        @auth
        <form method="POST" action="{{ route('logout') }}">
            @csrf

            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                this.closest('form').submit();">
                    {{ __('Logout') }}
            </a>
        </form>
        @endauth

        <nav>
            <ul>
                <li><a href="{{ route('home') }}">Posts</a></li>
                @auth
                <li><a href="{{ route('createPostPage') }}">New</a></li>
                @endauth
            </ul>
        </nav>
        
        <img src="{{ asset('assets/img/header.svg') }}" alt="header image" title="header image">
    </header>

    <main>
        @yield('content')
    </main>
    
    <footer><img src="{{ asset('assets/img/footer.svg') }}" alt="footer image" title="footer image"></footer>

    @yield('scripts')
</body>

</html>