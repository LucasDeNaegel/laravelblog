@extends('layouts.master')

@section('title')
    new post
@endsection

@section('css')
    <script src="https://cdn.tiny.cloud/1/r2bg2oj3vi6qvabyc34n6rehom99ic4a6p03rxb7ygks5s3j/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="{{ asset('assets/js/tiny.js') }}"></script>
@endsection

@section('content')
    <section>
        <h2>New Blog Post</h2>

        @if ($errors -> any())
            <ul class="error">
                @foreach ($errors -> all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <form action="{{route('makePost') }}" method="post">
            @csrf

            <fieldset>
                <legend>Main Content</legend>

                <label for="title">Title</label>
                <input type="text" id="title" name="title" required>

                <label for="content">Content</label>
                <textarea name="content" id="content"></textarea>

                <label for="type">Type</label>
                <input type="text" id="type" name="type" required>

                <label for="date">Date of event</label>
                <input type="date" id="date" name="date">

                <label for="location">Location of event</label>
                <input type="text" id="location" name="location">

                <label for="duration">Duration in hours</label>
                <input type="number" id="duration" name="duration">
            </fieldset>

            <fieldset id="mediaSet">
                <legend>Media</legend>

                <div id="media">
                    <div>
                        <label for="mediaUpload"></label>
                        <input type="file" name="media[0]" id="mediaUpload" accept="image/*,video/*">
                            
                        <label for="caption">Caption</label>
                        <input type="text" name="media[0][caption]" id="caption">

                        <a href="#" class="button">remove</a>
                    </div>
                </div>

                <a href="#">add</a>
            </fieldset>

            <fieldset id="refSet">
                <legend>References</legend>

                <div id="ref">
                    <div>
                        <label for="apa">Give APA part</label>
                        <input type="text" name="refs[0][apa]" id="apa">

                        <label for="url">Url</label>
                        <input type="url" name="refs[0][url]" id="url">

                        <a href="#" class="button">remove</a>
                    </div>
                </div>

                <a href="#">add</a>
            </fieldset>
            
            <input type="submit" value="submit">
        </form>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/register.js') }}"></script>
@endsection