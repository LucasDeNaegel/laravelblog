@extends('layouts.master')

@section('title')
    home
@endsection

@section('content')

    @if (count($posts) > 0)
        @foreach ($posts as $post)
            <article>

                <aside>
                    <h3>Info</h3>
                    <ul>
                        <li><strong>Type: </strong>{{ $post->type }}</li>
                        @if ($post->location)
                            <li><strong>Location: </strong>{{ $post->location }}</li>
                        @endif
                        @if ($post->date)
                            <li><strong>Date: </strong>{{ $post->date }}</li>
                        @endif
                        @if ($post->duration)
                            <li><strong>Duration: </strong>{{ $post->duration }}</li>
                        @endif
                    </ul>
                </aside>

                <h2>{{$post->title}}</h2>
                <p class="date"> <strong>created at:</strong> {{ $post->created_at->format('m/d/Y') }}</p>
                <p>{{ $post->preview }}</p> 

                <a href="{{route('postDetail', $post->id)}}">Read More</a>
            </article>
        @endforeach
    @else
        <p>There is no available content yet.</p>
    @endif

@endsection('content')