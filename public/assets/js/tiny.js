"use stict"

document.addEventListener("DOMContentLoaded", init);

function init() {
    tinymce.init({
        selector:'textarea',
        plugins: 'link help lists',
        menubar: false,
        toolbar: 'formatselect | bold italic | bullist numlist | link | help',
        default_link_target: '_blank'
    });
}