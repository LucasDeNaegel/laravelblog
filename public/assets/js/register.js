"use stict"

document.addEventListener("DOMContentLoaded", init);

let refId = 1;
let mediaId = 1;

function init() {
    document.querySelector("body").addEventListener("click", clickedOn);
}

function clickedOn(e) {
    if (e.target.innerHTML.toUpperCase() === "REMOVE") {
        e.preventDefault();
        remove(e.target.parentElement);
    } else if (e.target.innerHTML.toUpperCase() === "ADD") {
        e.preventDefault();
        add(e);
    }
}

function remove(toRemove) {
    const parent = toRemove.parentElement;
    parent.removeChild(toRemove);
}

function add(e) {
    const type = e.target.parentElement.id;
    if (type.toUpperCase() === "REFSET") {
        addRef();
    } else if (type.toUpperCase() === "MEDIASET" ) {
        addMedia();
    }
}

function addRef() {
    const html = 
        `<label for="apa${refId}">Give APA part</label>
        <input type="text" name="refs[${refId}][apa]" id="apa${refId}">

        <label for="url${refId}">Url</label>
        <input type="url" name="refs[${refId}][url]" id="url${refId}">

        <a href="#" class="button">remove</a>`;
    addElem("ref", "div", html);
    refId++;
}

function addMedia() {
    const html = 
        `<label for="mediaUpload${mediaId}"></label>
        <input type="file" name="media[${mediaId}]" id="mediaUpload${mediaId}" accept="image/*,video/*">
            
        <label for="caption${mediaId}">Caption</label>
        <input type="text" name="media[${mediaId}][caption]" id="caption${mediaId}">

        <a href="#" class="button">remove</a>`;
    addElem("media", "div", html);
    mediaId++;
}

function addElem(parrentId, wrapper, html) {
    const el = document.createElement(wrapper)
    el.innerHTML = html;
    document.getElementById(parrentId).appendChild(el);
}
