<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/post/new', [PostController::class, 'showCreationPage'])->middleware('auth')->name('createPostPage');
Route::get('/post/{id}', [PostController::class, 'showDetailPage'])->where('id', '[0-9]+')->name('postDetail');

Route::post('/post/new', [PostController::class, 'add'])->middleware('auth')->name('makePost');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');
