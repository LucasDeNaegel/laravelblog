<?php

namespace App\Modules\Ref\Services;

use App\Models\Ref;
use Illuminate\Support\Facades\Validator;

class RefService
{
    private $model = null;

    private $creationRules = [
        "ref.*.apa" => "required|string",
        "ref.*.url" => "required|active_url",
    ];

    private $updateRules = [
        "ref.*.apa" => "string",
        "ref.*.url" => "active_url",
    ];

    private $errors = null;

    public function __construct(Ref $model)
    {
        $this->model = $model;
    }

    public function update($id, $data)
    {
        $this->errors = null;
        $validator = Validator::make($data, $this->updateRules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return null;
        }
        
        $result = $this->change($id, $data);
        return $result;
    }

    public function add($data, $postId)
    {
        $this->errors = null;
        $validator = Validator::make($data, $this->creationRules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return null;
        }

        $result = $this->create($data, $postId);
        return $result;
    }

    public function findPostLinks($id)
    {
        $refs = $this->model->where('post', $id)->get();

        return $refs;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return !is_null($this->errors);
    }

    private function create($data, $postId)
    {
        foreach ($data as $entry) {
            $model = new $this->model;
            
            $model["apa"] = $entry["apa"];
            $model["url"] = $entry["url"];

            $model["post"] = $postId;
            $model->save();
        }
    }

    private function change($id, $data)
    {
        $oldModel = $this->model->find($id);

        $oldModel["apa"] = isset($data["apa"]) ? $data["apa"] : $oldModel["apa"];
        $oldModel["url"] = isset($data["url"]) ? $data["url"] : $oldModel["url"];

        $oldModel->save();
        
    }
}
