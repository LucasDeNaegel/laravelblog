<?php

namespace App\Modules\Post\Services;

use App\Models\Post;
use Illuminate\Support\Facades\Validator;
use Purifier;

class PostService
{
    private $model = null;

    private $creationRules = [
        "title" => "required|string",
        "content" => "required|string",
        
        "date" => "nullable|date",
        "location" => "nullable|string",
        "type" => "required|string",
        "duration" => "nullable|integer",
    ];

    private $updateRules = [
        "title" => "string",
        "content" => "string",
        
        "date" => "nullable|date",
        "location" => "nullable|string",
        "type" => "string",
        "duration" => "nullable|integer",
    ];

    private $errors = null;

    public function __construct(Post $model) {
        $this->model = $model;
    }

    public function update($id, $data) {
        $this->errors = null;
        $validator = Validator::make($data, $this->updateRules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return null;
        }
        
        $result = $this->change($id, $data);
        return $result;
    }

    public function add($data) {
        $this->errors = null;
        $validator = Validator::make($data, $this->creationRules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return null;
        }

        $result = $this->create($data);
        return $result;
    }

    public function find($id)
    {
        $post = $this->model->find($id);
        $post["media"] = $post->media;
        $post["refs"] = $post->refs;

        return $post;
    }

    public function all() {
        
        $result = $this->model->all();

        return $result;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function hasErrors() {
        return !is_null($this->errors);
    }

    private function create($data) {
        $model = new $this->model;

        $model["title"] = $data["title"];
        $model["content"] = Purifier::clean($data["content"]);

        $model['date'] = isset($data['date']) ? $data['date'] : null;
        $model['location'] = isset($data['location']) ? $data['location'] : null;
        $model['type'] = isset($data['type']) ? $data['type'] : null;
        $model['duration'] = isset($data['duration']) ? $data['duration'] : null;


        $model->save();
        $model->refresh();

        return $model;
    }

    private function change($id, $data) {
        $post = $this->model->find($id);

        $post["title"] = isset($data["title"]) ? $data["title"] : $post["title"];
        $post["content"] = isset($data["content"]) ? Purifier::clean($data["content"]) : $post["content"];
        $post["date"] = isset($data["date"]) ? $data["date"] : $post["date"];
        $post['location'] = isset($data['location']) ? $data['location'] : $post["location"];
        $post['type'] = isset($data['type']) ? $data['type'] : $post['type'];
        $post['duration'] = isset($data['duration']) ? $data['duration'] : $post['duration'];

        $post->save();
        $post->refresh();

        return $post;
    }
}
