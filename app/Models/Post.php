<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $appends = ['preview'];

    public function media()
    {
        return $this->hasMany('App\Models\Media', 'post');
    }

    public function refs()
    {
        return $this->hasMany('App\Models\Ref', 'post');
    }

    public function getPreviewAttribute() 
    {
        $splitContent = explode("</p>", $this->attributes["content"], 2);
        $preview = $splitContent[0];
        return strip_tags($preview);
    }
}
