<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Post\Services\PostService;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    function index(PostService $service) {
        $posts = $service->all();
        return view('home', ['posts' => $posts]);
    }
}
