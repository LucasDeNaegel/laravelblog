<?php

namespace App\Http\Controllers;

use App\Modules\Post\Services\PostService;
use App\Modules\Ref\Services\RefService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    function showDetailPage($id, PostService $postService) {

        $post = $postService->find($id);

        return view('detailPost', ['post' => $post]);
    }
    
    function showCreationPage() {
        return view('createPost');
    }

    function add(Request $req, PostService $postService, RefService $refService) {
        if (Auth::check()) {
            $data = $req->all();
            $post = $this->createPost($data, $postService);
            $this->createRefs($data, $post->id, $refService);
        }
        return redirect('/');
    }

    function createPost($data, $service) {
        $post = $service->add($data);
        if ($service->hasErrors()) {
            return view('createPost', ['errors' => $service->getErrors()]);
        }
        return $post;
    }

    function createRefs($data, $postId, $service) {
        if ( $this->dataHasRefs($data) ) {
            
            $service->add($data["refs"], $postId);

            if ($service->hasErrors()) {
                return view('createPost', ['errors' => $service->getErrors()]);
            }
        }
    }

    function dataHasRefs($data) {
        return array_key_exists ("refs", $data) && $data["refs"][0]["apa"] && $data["refs"][0]["url"];
    }
}
